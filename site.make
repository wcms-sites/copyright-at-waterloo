core = 7.x
api = 2

; Frequently Asked Questions
projects[faq][type] = "module"
projects[faq][download][type] = "git"
projects[faq][download][url] = "https://git.uwaterloo.ca/drupal-org/faq.git"
projects[faq][download][tag] = "7.x-1.0-rc2-uw_wcms1"
projects[faq][subdir] = ""
